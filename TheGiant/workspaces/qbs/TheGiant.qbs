import qbs

CppApplication {
    type: "application" // To suppress bundle generation on Mac
    consoleApplication: true
    destinationDirectory: "../../../__build-output"

    cpp.cxxFlags: {
        if (qbs.toolchain.contains("gcc")) {
            return [ "-std=c++14" ]
        }
        else if (qbs.toolchain.contains("clang")) {
            return [ "-std=c++14"]
        }
    }

    cpp.linkerFlags:  {
        if (qbs.targetOS.contains("windows")) {
            if (qbs.toolchain.contains("msvc")) {
                return [ "/subsystem:windows" ]
            }
        }
    }

    cpp.staticLibraries: {
        if (qbs.targetOS.contains("windows")) {
            if (qbs.toolchain.contains("msvc")) {
                print("Loading static libraries for: Windows MSCV!")
                return [
                            "User32.lib",
                            "kernel32.lib",
                            "gdi32.lib"
                        ];
            }
            else if (qbs.toolchain.contains("mingw")) {
                print("Loading static libraries for: Windows MinGW!")
                return [
                            "user32",
                            "kernel32",
                            "kernel32"
                        ];
            }
            else {
                print("Invalid toolchain!");
            }
        }
        else if (qbs.targetOs.contains("linux")) {
            if (qbs.toolchain.contains("gcc")) {
                print("Linux GCC is not currently supported...")
            }
            else if (qbs.toolchain.contains("clang")) {
                print("Linux Clang is not currently supported...")
            }
        }
        else {
            print("Unsupported Target OS...")
            return []
        }
    }

    files: [
        "../../src/main.cpp",
        "../../src/video/windows.cpp",
        "../../src/video/windows.h",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
    }
}

