#include <iostream>

#include <Windows.h>


#define UNUSED(x) (void)(x)       // Unused param (C compatible - not applicable to expressions)


int CALLBACK
WinMain (HINSTANCE hInstance,
         HINSTANCE /*hPrevInst*/, // Unused param (C++ only)
         LPSTR lpCmdLine,
         int (nShowCmd))
{
    UNUSED(hInstance);
//    UNUSED(hPrevInst);
    UNUSED(lpCmdLine);
    UNUSED(nShowCmd); // This param is used

    std::cout << "nShowCmd = " << nShowCmd << std::endl;
    std::cout << "Now making my first Windows window!" << std::endl;

    MessageBoxW (NULL,
                 L"Hello World!",
                 L"hello",
                 MB_OK | MB_ICONINFORMATION);

    return 0;
}
